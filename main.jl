using CSV, DataFrames
using Statistics

#client prediction of term deposit

#load data
df = CSV.read("./bank-additional-full.csv")
#renaming column
rename!(df,Dict(:y => :deposit))

select!(df,Not(:default))
select!(df,Not(:contact))
select!(df,Not(:job))
select!(df,Not(:age))
select!(df,Not(:previous))



dfmat=convert(Matrix,df)

#(1)
#DATA CLEANING
for i = 1:(size(dfmat)[2])
    if(i == size(dfmat)[2])
        for j = 1:size(dfmat)[1]
            if(dfmat[j, i] == "yes")
                dfmat[j, i] = Int(1)
            else
                dfmat[j, i] = Int(0)
            end
        end
    else
        uniquee = unique(dfmat[:,i])
        for j = 1:size(dfmat)[1]
            for k = 1:size(uniquee)[1]
                    if(dfmat[j,i] == uniquee[k])
                        dfmat[j,i] = Int(k)
                    end
            end
        end
    end
end

# function for standadising the x train
function features_scaling(x_train)   
    meann = mean(x_train, dims =1)
    standard_deviation = std(x_train,dims =1)
    standardised_x = (x_train .- meann) ./ standard_deviation
    return (standardised_x , meann , standard_deviation)
end


#function for normalising the x test 
function transform_features(x_test ,meann , standard_deviation)
    transforned_x = (x_test .- meann) ./ standard_deviation
    return transforned_x

end



#separate training and testing sets
proportion = ceil(Int, size(dfmat, 1) *0.8)

train_set = dfmat[1: proportion, 1: end-1]
train_outcome = dfmat[1: proportion, end]

test_set = convert(Matrix, dfmat[proportion+1: end, 1: end-1])
test_outcome = dfmat[proportion+1: end, end]


#scaling the  X train #
train_set, meann ,standard_deviation = features_scaling(train_set)

#normalising the x tesing
test_set = transform_features(test_set,meann , standard_deviation)



###############################

#(2)
#regularized logistic regression algorithm
#h(x) = 0x => linear
#h(x) = g(0Tx) = 1/1+e^-0Tx 
#h = g(transpose(theta).training_set) #h = estimate
function h(x)
    return g(x)
end

# g(z) = 1/1+e^-z => logistic of sigmoid function
function g(z)
    return (1.0 / (1.0 .+ exp(-z[1])))
end
#derivative of g(z) equals g(z)(1-g(z))

#cost function
function cost(actual, estimate)
    return (-(actual*log(estimate) + (1 - actual)*log(1-estimate))) 
end

###############################

#gradient descent
#0 = 0 - rete*changeinJ(0)
function descend(input, output, rate)
    thet = zeros(Float32, 1, size(input, 2))
    for x in 1: size(input, 1)
        thet = thet .- (rate .* transpose(derivative(h(thet .* input[x, :]), output[x], input[x, :])) .+ 0.01 .* broadcast(abs, thet))
    end 
    return thet
end

#changeinJ(0) = (h(x)-y)x
function derivative(estimate, actual, feature)
    return (estimate - actual) .* feature
end

#ts = train_set
#to = train_outcome
function compute(ts, to)
    return descend(ts, to, 0.1)
end

#(3)
#implementation of classifier
function predict(ts, to, thet)
    true_p = 0
    true_n = 0
    false_p = 0
    false_n = 0

    threshold = 0.5
    total = 0

    for x in 1: size(ts, 1)
        t = ts[x, :]
        res = to[x]
        hypotheses = h(thet .* t)
        answer = 0

        if(hypotheses >= threshold)
            answer = 1
        end

        if(answer == 1 && res == 1)
            true_p += 1
        end

        if(answer == 0 && res == 1)
            false_n += 1
        end

        if(answer == 0 && res == 0)
            true_n += 1
        end

        if(answer == 1 && res == 0)
            false_p += 1
        end

    end

    println("accuracy: ", ((true_p+true_n)/(true_p+true_n+false_n+false_p))* 100, "   ", (true_p+true_n), " out of ", (true_p+true_n+false_n+false_p))    
    println("recall: ", (true_p/(true_p+false_n))* 100, "   ", true_p, " out of ", (true_p+false_n))
    println("precision: ", (true_p/(true_p+false_p))* 100, "   ", true_p, " out of ", (true_p+false_p))    

end

theta = compute(train_set, train_outcome)

predict(test_set, test_outcome, theta)
